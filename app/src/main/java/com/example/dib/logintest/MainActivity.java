package com.example.dib.logintest;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.dib.logintest.activity.HomeActivity;
import com.example.dib.logintest.activity.RegistrationActivity;
import com.example.dib.logintest.activity.ResetPasswordActivity;
import com.example.dib.logintest.base.BaseToolbarActivity;
import com.example.dib.logintest.utils.ConnectionChecker;
import com.example.dib.logintest.utils.ValidationHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import butterknife.BindView;
import butterknife.OnClick;


public class MainActivity extends BaseToolbarActivity {


    private ConnectionChecker cc;

    @BindView(R.id.sign_in_button)
    Button siginBtn;
    @BindView(R.id.sign_up_button)
    Button signupBtn;
    @BindView(R.id.forgot_password_button)
    Button forgotPassBtn;

    @BindView(R.id.til_email)
    TextInputLayout emailTil;
    @BindView(R.id.til_password)
    TextInputLayout passwordTil;

    @BindView(R.id.email)
    TextInputEditText emailTiet;
    @BindView(R.id.password)
    TextInputEditText passwordTiet;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cc = new ConnectionChecker();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setBaseTitle("Login");
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @OnClick({R.id.sign_in_button, R.id.sign_up_button, R.id.forgot_password_button})
    void onClickViews(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                loginToApp();
                break;
            case R.id.sign_up_button:
                startActivity(new Intent(MainActivity.this, RegistrationActivity.class));
                break;
            case R.id.forgot_password_button:
                startActivity(new Intent(MainActivity.this, ResetPasswordActivity.class));
                break;
            default:
                break;
        }

    }

    private void loginToApp() {
        ValidationHelper vh = new ValidationHelper();

        vh.addInputView(emailTil, ValidationHelper.ValidationType.EMAIL);
        vh.addInputView(passwordTil, ValidationHelper.ValidationType.PASSWORD);

        if (vh.validate() && cc.netConnect(getApplicationContext())) {
            String email = emailTiet.getText().toString();
            final String password = passwordTiet.getText().toString();
            progressBar.setVisibility(View.VISIBLE);
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressBar.setVisibility(View.GONE);
                            if (!task.isSuccessful()) {
                                task.getException();
                                showToast("" + task.getException().getMessage());
                            } else {
                                showToast("Authentication completed");
                                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                finish();
                            }
                        }
                    });

        } else if (!cc.netConnect(getApplicationContext())) {
            showToast("Check your internet connection");
        }

    }
}
