package com.example.dib.logintest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dib.logintest.R;
import com.example.dib.logintest.utils.ValidationHelper;
import com.example.dib.logintest.base.BaseNavigationActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by fur3ll on 1/3/2018.
 */

public class ChangePasswordActivity extends BaseNavigationActivity {

    @BindView(R.id.til_old_password)
    TextInputLayout oldpasswordTil;
    @BindView(R.id.til_new_password)
    TextInputLayout newpasswordTil;

    @BindView(R.id.old_password)
    TextInputEditText oldpasswordTiet;
    @BindView(R.id.new_password)
    TextInputEditText newpasswordTiet;

    @BindView(R.id.save_button)
    Button saveBtn;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_change_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavTitle("Change Password");
    }

    @OnClick(R.id.save_button)
    public void onClick(View view){
        ValidationHelper vh = new ValidationHelper();

        vh.addInputView(oldpasswordTil, ValidationHelper.ValidationType.PASSWORD);
        vh.addInputView(newpasswordTil,ValidationHelper.ValidationType.PASSWORD);

        if(vh.validate()){
            changePass();
        }
    }

    void changePass(){
        final String newpassword = newpasswordTiet.getText().toString();
        final String oldpassword = oldpasswordTiet.getText().toString();
        final String email = mCurrentUser.getEmail();
        if (!oldpassword.equals(newpassword)){
            AuthCredential credential = EmailAuthProvider.getCredential(email,oldpassword);

            mCurrentUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        mCurrentUser.updatePassword(newpassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()){
                                    showToast("Something went wrong. Please try again later");
                                }else {
                                    showToast("Password successfully changed");
                                    startActivity(new Intent(ChangePasswordActivity.this, HomeActivity.class));
                                    finish();
                                }
                            }
                        });
                    }else {
                        showToast("Authentication Failed");
                    }
                }
            });


        }else {
            showToast("New password same as old");
        }

    }
}