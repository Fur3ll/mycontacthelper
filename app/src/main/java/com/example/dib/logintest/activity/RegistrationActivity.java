package com.example.dib.logintest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.dib.logintest.R;
import com.example.dib.logintest.base.BaseNavigationActivity;
import com.example.dib.logintest.base.BaseToolbarActivity;
import com.example.dib.logintest.utils.ConnectionChecker;
import com.example.dib.logintest.utils.ValidationHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;


public class RegistrationActivity extends BaseToolbarActivity {

    private EditText inputEmail, inputPassword;
    private Button btnSignUp;
    private TextInputLayout mEmailLayout, mPasswordLayout;
    private ProgressBar progressBar;
    private ConnectionChecker cc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBaseTitle("Sign up");

        initView();
        cc = new ConnectionChecker();


        if(cc.netConnect(getApplicationContext()) == false){
            showToast("Check your internet connection");
        }


         btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ValidationHelper vh = new ValidationHelper();

                vh.addInputView(mEmailLayout, ValidationHelper.ValidationType.EMAIL);
                vh.addInputView(mPasswordLayout, ValidationHelper.ValidationType.PASSWORD);

                if(vh.validate()&&cc.netConnect(getApplicationContext())){

                    String email = inputEmail.getText().toString();
                    final String password = inputPassword.getText().toString();

                    progressBar.setVisibility(View.VISIBLE);

                    mAuth.createUserWithEmailAndPassword(email,password)
                            .addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    showToast("createUserWithEmail:onComplete:" + task.isSuccessful());
                                    progressBar.setVisibility(View.GONE);
                                    if (!task.isSuccessful()){
                                        task.getException();
                                        showToast("" + task.getException().getMessage());
                                    }
                                    else{
                                        showToast("Authentication completed");
                                        startActivity(new Intent(RegistrationActivity.this, BaseNavigationActivity.class));
                                        finish();
                                    }
                                }
                            });

                }else if(!cc.netConnect(getApplicationContext())){

                    showToast("Check your internet connection");
                }
            }
        });


    }

    private void initView() {
        btnSignUp = (Button) findViewById(R.id.sign_up_button);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        mEmailLayout = (TextInputLayout) findViewById(R.id.til_email);
        mPasswordLayout = (TextInputLayout) findViewById(R.id.til_password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_registration;
    }


}
