package com.example.dib.logintest.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dib.logintest.MainActivity;
import com.example.dib.logintest.R;
import com.example.dib.logintest.activity.AddContactsActivity;
import com.example.dib.logintest.activity.HomeActivity;
import com.example.dib.logintest.activity.ProfileActivity;
import com.example.dib.logintest.utils.CircleTransform;
import com.example.dib.logintest.utils.Imageutils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;


public abstract class BaseNavigationActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, Imageutils.ImageAttachmentListener{


    @BindView(R.id.nav_toolbar)
    Toolbar baseNavToolbar;

    @BindView(R.id.title_tv)
    TextView mTitle;


    protected ActionBarDrawerToggle mToggle;
    ImageView mUserBlurPhoto;
    ImageView userPhoto;
    ViewGroup mNavHeader;
    BlurView mNavBlurView;
//    ImageButton mUserPhotoImageBtn;
    TextView mUserEmail;
    TextView mUserName;
    Imageutils imageutils;
    DrawerLayout drawer;
    Uri mUserPhoto;
    Intent mActivityToSet = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_navigation);
        getLayoutInflater().inflate(getLayoutResourceId(), (ViewGroup) findViewById(R.id.container), true);

        ButterKnife.bind(this);

        imageutils = new Imageutils(this);

        drawer = findViewById(R.id.drawer_layout);

        mToggle = new ActionBarDrawerToggle(
                this, drawer, baseNavToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                if(mActivityToSet != null){
                    startActivity(mActivityToSet);
                    mActivityToSet = null;
                    finish();
                }
                super.onDrawerClosed(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        };

        drawer.addDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        mUserEmail = (TextView) header.findViewById(R.id.user_email_tv);
        mUserName = (TextView) header.findViewById(R.id.user_name_tv);
        mUserName.setText(mCurrentUser.getDisplayName());
        mUserEmail.setText(mCurrentUser.getEmail());

        userPhoto = (ImageView) header.findViewById(R.id.user_photo_iv);
        userPhoto.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_contacts));
        mUserBlurPhoto = (ImageView) header.findViewById(R.id.user_blur_photo_iv);

        mUserPhoto = mCurrentUser.getPhotoUrl();

        if (mUserBlurPhoto != null){
            updateUserPic();
        }

        mNavHeader = (ViewGroup) header.findViewById(R.id.nav_header);
        mNavBlurView = (BlurView) header.findViewById(R.id.blurView);
        setupBlurView();

    }



    protected void setNavTitle(String text) {
        mTitle.setText(text);
    }

    protected abstract int getLayoutResourceId();



    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        else if (getLayoutResourceId() != R.layout.activity_home) {
            startActivity(new Intent(BaseNavigationActivity.this, HomeActivity.class));
            finish();
        }
        else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.


        switch (item.getItemId()){

            case R.id.nav_add:
                if(getLayoutResourceId() != R.layout.activity_add_contacts){
                    mActivityToSet = new Intent(BaseNavigationActivity.this, AddContactsActivity.class);
                }
                break;

            case R.id.nav_name:
                if(getLayoutResourceId() != R.layout.activity_profile){
                    mActivityToSet = new Intent(BaseNavigationActivity.this,ProfileActivity.class);
                }
                break;
            case R.id.nav_my_contacts:
                if(getLayoutResourceId() != R.layout.activity_home){
                    mActivityToSet = new Intent(BaseNavigationActivity.this, HomeActivity.class);
                }
                break;
            case R.id.nav_logout:
                mAuth.signOut();
                mActivityToSet = new Intent(BaseNavigationActivity.this, MainActivity.class);
                break;
            default:
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    private void setupBlurView(){
        final float radius = 10f;


        final Drawable windowBackground = getWindow().getDecorView().getBackground();


        final BlurView.ControllerSettings topView = mNavBlurView.setupWith(mNavHeader)
                .windowBackground(windowBackground)
                .blurAlgorithm(new RenderScriptBlur(this))
                .blurRadius(radius);

    }

    @Override
    public void imageAttachment(int from, String filename, Bitmap file, Uri uri) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        imageutils.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        imageutils.request_permission_result(requestCode, permissions, grantResults);
    }


    public void updateUserPic(){

        Picasso.with(this)
                .load(mCurrentUser.getPhotoUrl())
                .transform(new CircleTransform())
                .fit()
                .centerInside()
                .into(userPhoto);

        Picasso.with(this)
                .load(mCurrentUser.getPhotoUrl())
                .fit()
                .centerInside()
                .into(mUserBlurPhoto);

    }

    public void updateNavDraw(){
        mNavHeader.invalidate();
    }
}

