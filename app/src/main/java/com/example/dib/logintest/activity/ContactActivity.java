package com.example.dib.logintest.activity;

import android.content.Intent;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.dib.logintest.R;
import com.example.dib.logintest.base.BaseNavigationActivity;
import com.example.dib.logintest.model.UserModel;
import com.example.dib.logintest.utils.CircleTransform;
import com.example.dib.logintest.utils.Imageutils;
import com.example.dib.logintest.utils.ValidationHelper;
import com.google.firebase.database.DataSnapshot;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

public class ContactActivity extends BaseNavigationActivity {

    @BindView(R.id.delete_contact_button)
    Button deleteContact;
    @BindView(R.id.save_button)
    Button saveButton;

    @BindView(R.id.til_email)
    TextInputLayout emailTil;
    @BindView(R.id.til_phone)
    TextInputLayout phoneTil;
    @BindView(R.id.til_name)
    TextInputLayout nameTil;

    @BindView(R.id.email)
    TextInputEditText emailTiet;
    @BindView(R.id.phone)
    TextInputEditText phoneTiet;
    @BindView(R.id.name)
    TextInputEditText nameTiet;

    @BindView(R.id.contact_iv)
    ImageView contactIv;

    @BindView(R.id.contact_ib)
    ImageButton contactBtn;

    UserModel contact;
    String photo;
    String name;
    String email;
    String phone;
    String key;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_contact;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavTitle("Contact");


        contact = getIntent().getParcelableExtra(UserModel.class.getCanonicalName());
        nameTiet.setText(contact.getName());
        emailTiet.setText(contact.getEmail());
        phoneTiet.setText(contact.getPhone());

        photo = contact.getPhoto();
        key = contact.getKey();

        Picasso.with(ContactActivity.this)
                .load(photo)
                .transform(new CircleTransform())
                .fit()
                .centerInside()
                .into(contactIv);


    }

    @OnClick({R.id.contact_ib,R.id.delete_contact_button,R.id.save_button})
    void onClickViews (View view){

        switch (view.getId()){
            case R.id.contact_ib:
                break;
            case R.id.delete_contact_button:

                String currentUserID = mCurrentUser.getUid();
                mDatabase.child(currentUserID).child(key).removeValue();
                showToast("Contact successfully deleted");
                startActivity(new Intent(ContactActivity.this, HomeActivity.class));
                finish();
                break;
            case R.id.save_button:
                break;
            default:
                break;
        }
    }

}
