package com.example.dib.logintest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dib.logintest.MainActivity;
import com.example.dib.logintest.R;
import com.example.dib.logintest.base.BaseToolbarActivity;
import com.example.dib.logintest.utils.ConnectionChecker;
import com.example.dib.logintest.utils.ValidationHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


public class ResetPasswordActivity extends BaseToolbarActivity {

    private EditText inputEmail;
    private Button btnReset;
    private TextInputLayout mEmailLayout;
    private String mTitle;
    private ConnectionChecker cc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBaseTitle("Reset Password");

        inputEmail = (EditText) findViewById(R.id.email);
        mEmailLayout = (TextInputLayout) findViewById(R.id.til_email);
        btnReset = (Button) findViewById(R.id.reset_button);
        cc = new ConnectionChecker();

        if(cc.netConnect(getApplicationContext()) == false){
            showToast("Check your internet connection");
        }

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ValidationHelper vh = new ValidationHelper();
                vh.addInputView(mEmailLayout, ValidationHelper.ValidationType.EMAIL);

                if(vh.validate()&& cc.netConnect(getApplicationContext())){
                    String email = inputEmail.getText().toString();
                    mAuth.sendPasswordResetEmail(email).
                            addOnCompleteListener(ResetPasswordActivity.this, new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(!task.isComplete()){
                                        task.getException();
                                        showToast("" + task.getException().getMessage());
                                    }else {
                                        showToast("Check your inbox for a password reset email");
                                        startActivity(new Intent(ResetPasswordActivity.this, MainActivity.class));
                                        finish();
                                    }
                                }
                            });
                }else if(!cc.netConnect(getApplicationContext())){
                    showToast("Check your internet connection");
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_reset_password;
    }
}
