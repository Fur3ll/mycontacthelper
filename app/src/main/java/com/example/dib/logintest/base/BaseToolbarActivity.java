package com.example.dib.logintest.base;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dib.logintest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fur3ll on 12/29/2017.
 */

public abstract class BaseToolbarActivity extends BaseActivity {

    @BindView(R.id.nav_toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title_tv)
    TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_toolbar);
        getLayoutInflater().inflate(getLayoutResourceId(), (ViewGroup) findViewById(R.id.rl_content), true);
        ButterKnife.bind(this);
        setupToolbar();
    }

    protected void setBaseTitle(String text){
        mTitle.setText(text);
        String main = "Login";
        if (main.equals(text)){
            mTitle.setPadding(116,0,0,0);
        }
    }

    protected abstract int getLayoutResourceId();

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp (){
        onBackPressed();
        return true;
    }

}
