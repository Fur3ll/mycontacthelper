package com.example.dib.logintest.utils;

// property to nest software
// package com.nest.whearthat.utils;

        import android.support.annotation.NonNull;
        import android.support.annotation.Nullable;
        import android.support.design.widget.TextInputLayout;
        import android.text.Editable;
        import android.text.TextUtils;
        import android.text.TextWatcher;
        import android.util.Patterns;
        import android.view.View;
        import android.widget.EditText;

        import com.example.dib.logintest.R;

        import java.util.ArrayList;
        import java.util.List;



public class ValidationHelper {

    public enum ValidationType {
        MANDATORY, PASSWORD, EMAIL, PHONE
    }

    private boolean isInputDataCorrect;
    private List<ValidationObjectModel> list = new ArrayList<>();

    public ValidationHelper() {
        isInputDataCorrect = true;
        list.clear();
    }

    public void refreshValidationHelper() {
        isInputDataCorrect = true;
        list.clear();
    }

    public void addInputView(EditText inputView, ValidationType validationType) {
        list.add(new ValidationObjectModel(inputView, validationType));
    }

    public void addInputView(TextInputLayout inputView, ValidationType validationType) {
        list.add(new ValidationObjectModel(inputView, validationType));
    }

    public boolean validate() {
        isInputDataCorrect = true;
        for (ValidationObjectModel vom : list) {
            View inputView = vom.getInputView();
            ValidationType validationType = vom.getValidationType();
            EditText editTextInput = null;
            if (inputView instanceof EditText) {
                editTextInput = (EditText) inputView;
            } else if (inputView instanceof TextInputLayout) {
                editTextInput = ((TextInputLayout) inputView).getEditText();
            }
            boolean iterationResult = true;
            switch (validationType) {
                case MANDATORY:
                    iterationResult = checkMandatory(editTextInput.getText().toString(), inputView);
                    break;
                case PASSWORD:
                    iterationResult = checkPassword(editTextInput.getText().toString(), inputView);
                    break;
                case EMAIL:
                    iterationResult = checkEmail(editTextInput.getText().toString(), inputView);
                    break;
                case PHONE:
                    iterationResult = checkPhone(editTextInput.getText().toString(),inputView);
                    break;
            }
            if (!iterationResult) {
                isInputDataCorrect = false;
            }
        }
        return isInputDataCorrect;
    }

    public boolean checkEmail(@NonNull String email, @NonNull View inputView) {
        boolean result = false;
        if (checkMandatory(email, inputView)) {
            result = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
            if (!result) {
                setError(inputView.getContext().getString(R.string.error_email), inputView);
            }
        }
        if (result) {
            setErrorNull(inputView);
        }
        return result;
    }

    public boolean checkPhone(@Nullable String phone, @Nullable View inputView){
        boolean result = false;
        if (checkMandatory(phone, inputView)){
            result = Patterns.PHONE.matcher(phone).matches();
            if(!result){
                setError(inputView.getContext().getString(R.string.error_phone), inputView);
            }
        }
        return result;
    }

    public boolean checkPassword(@NonNull String pass, @NonNull View inputView) {
        boolean result = false;
        if (checkMandatory(pass, inputView)) {
            result = pass.length() >= 2;
            if (!result) {
                setError(inputView.getContext().getString(R.string.error_pass), inputView);
            }
        }
        if (result) {
            setErrorNull(inputView);
        }
        return result;
    }

    public boolean checkMandatory(@NonNull String value, @NonNull View inputView) {
        boolean result;
        if (TextUtils.isEmpty(value)) {
            result = false;
            setError(inputView.getContext().getString(R.string.error_mandatory), inputView);
        } else {
            result = true;
        }
        if (result) {
            setErrorNull(inputView);
        }
        return result;
    }

    private void setErrorNull(@NonNull View inputView) {
        if (inputView instanceof EditText) {
            ((EditText) inputView).setError(null);
        } else if (inputView instanceof TextInputLayout) {
            ((TextInputLayout) inputView).setError(null);
            ((TextInputLayout) inputView).setErrorEnabled(false);
        }
    }

    private void setError(@NonNull String error, @NonNull View inputView) {
        if (inputView instanceof EditText) {
            final EditText editText = (EditText) inputView;
            editText.setError(error);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    setErrorNull(editText);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus && editText.getError() != null) {
                        setErrorNull(editText);
                    }
                }
            });
        } else if (inputView instanceof TextInputLayout) {
            final TextInputLayout textInputLayout = (TextInputLayout) inputView;
            textInputLayout.setError(error);
            textInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    setErrorNull(textInputLayout);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    private class ValidationObjectModel {

        private View inputView;
        private ValidationType validationType;

        public ValidationObjectModel(View inputView, ValidationType validationType) {
            this.validationType = validationType;
            this.inputView = inputView;
        }

        public View getInputView() {
            return inputView;
        }

        public void setInputView(View inputView) {
            this.inputView = inputView;
        }

        public ValidationType getValidationType() {
            return validationType;
        }

        public void setValidationType(ValidationType validationType) {
            this.validationType = validationType;
        }
    }
}
