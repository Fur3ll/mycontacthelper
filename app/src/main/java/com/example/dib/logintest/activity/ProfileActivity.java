package com.example.dib.logintest.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.dib.logintest.R;
import com.example.dib.logintest.utils.CircleTransform;
import com.example.dib.logintest.utils.Imageutils;
import com.example.dib.logintest.utils.ValidationHelper;
import com.example.dib.logintest.base.BaseNavigationActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by fur3ll on 1/3/2018.
 */

public class ProfileActivity extends BaseNavigationActivity {


    @BindView(R.id.til_name)
    TextInputLayout nameTil;
    @BindView(R.id.name)
    TextInputEditText nameTiet;


    @BindView(R.id.til_email)
    TextInputLayout emailTil;
    @BindView(R.id.email)
    TextInputEditText emailTiet;


    @BindView(R.id.save_button)
    Button saveBtn;
    @BindView(R.id.change_pass_button)
    Button changPassBtn;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.profile_iv)
    ImageView profileIv;
    @BindView(R.id.profile_ib)
    ImageButton profileBtn;

    Imageutils imageutils;
    Uri photoUri;
    Uri mPicUri;




    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_profile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavTitle("My profile");

        mStorage = FirebaseStorage.getInstance().getReference();
        nameTiet.setText(mCurrentUser.getDisplayName());
        emailTiet.setText(mCurrentUser.getEmail());
        emailTiet.setKeyListener(null);
        imageutils = new Imageutils(this);

        if (mCurrentUser.getPhotoUrl() != null){
            Picasso.with(this)
                    .load(mCurrentUser.getPhotoUrl())
                    .transform(new CircleTransform())
                    .fit()
                    .centerInside()
                    .into(profileIv);
        }
    }

    @OnClick({R.id.save_button,R.id.change_pass_button,R.id.profile_ib})
    void onClickViews (View view){


        switch (view.getId()){
            case R.id.save_button:

                ValidationHelper vh = new ValidationHelper();

                vh.addInputView(nameTil, ValidationHelper.ValidationType.MANDATORY);

                if (vh.validate()){
                    changeName();

                }
                progressBar.setVisibility(View.VISIBLE);
                updateUserPhoto();

                break;
            case R.id.change_pass_button:
                startActivity(new Intent(ProfileActivity.this, ChangePasswordActivity.class));
                finish();
                break;

            case R.id.profile_ib:

                if(imageutils.isDeviceSupportCamera())
                    imageutils.imagepicker(1);
                break;


            default:
                break;
        }
    }


    @Override
    public void imageAttachment(int from, String filename, Bitmap file, Uri uri) {

        Picasso.with(this)
                .load(uri)
                .transform(new CircleTransform())
                .fit()
                .centerInside()
                .into(profileIv);

        mPicUri = uri;

    }

    void updateUserPhoto (){

        if (mPicUri != null){
            StorageReference newStorage = mStorage.child("User photo").child(mPicUri.getLastPathSegment());
            newStorage.putFile(mPicUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!urlTask.isSuccessful());

                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setPhotoUri(urlTask.getResult())
                            .build();

                    mCurrentUser.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    showToast("Upload Successful");
                                    progressBar.setVisibility(View.GONE);
                                    updateUserPic();
                                }
                            });
                }
            });
        }

    }



    void changeName(){

        final String name = mCurrentUser.getDisplayName();
        final String newname = nameTiet.getText().toString();

        if(!name.equals(newname)){
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(newname)
                    .build();
            mCurrentUser.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(ProfileActivity.this, "Name changed",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }

}
