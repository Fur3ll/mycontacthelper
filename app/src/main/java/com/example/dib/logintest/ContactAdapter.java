package com.example.dib.logintest;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dib.logintest.activity.ContactActivity;
import com.example.dib.logintest.model.UserModel;
import com.example.dib.logintest.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactVH>{


    Uri contactPhoto;
    public List<UserModel> userContacts = new ArrayList<>();

    UserModel contact = new UserModel();
    Context context;

    public ContactAdapter(List<UserModel> userContacts,Context context){
        this.userContacts = userContacts;
        this.context = context;
    }


    @Override
    public ContactVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_card_view,parent,false);
        ContactVH contactVH = new ContactVH(v);
        return contactVH;
    }

    @Override
    public void onBindViewHolder(final ContactVH holder, int position) {
        final UserModel item = userContacts.get(position);
        holder.contactName.setText(item.getName());
        holder.contactPhone.setText(item.getPhone());
        holder.contactEmail.setText(item.getEmail());

        Picasso.with(holder.contactPhoto.getContext())
                .load(item.getPhoto())
                .transform(new CircleTransform())
                .fit()
                .centerCrop()
                .into(holder.contactPhoto);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ContactActivity.class);
                intent.putExtra(UserModel.class.getCanonicalName(),item);
                intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TASK|intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


        holder.callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callUser = new Intent(Intent.ACTION_CALL);
                callUser.setData(Uri.parse("tel:" + item.getPhone()));
                if(ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE)
                        == PackageManager.PERMISSION_GRANTED)
                    context.startActivity(callUser);
            }
        });

    }

    @Override
    public int getItemCount() {
        return userContacts.size();
    }

    @Override
    public void onAttachedToRecyclerView (RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }

    class ContactVH extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView contactPhoto;
        TextView contactName;
        TextView contactEmail;
        TextView contactPhone;
        ImageButton callButton;

        public ContactVH(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.contact_cv);
            contactPhoto = (ImageView) itemView.findViewById(R.id.photo_iv);
            contactName = (TextView) itemView.findViewById(R.id.contact_name_tv);
            contactEmail = (TextView) itemView.findViewById(R.id.contact_email_tv);
            contactPhone = (TextView) itemView.findViewById(R.id.contact_phone_tv);
            callButton = (ImageButton) itemView.findViewById(R.id.call_ib);
        }
    }
}
