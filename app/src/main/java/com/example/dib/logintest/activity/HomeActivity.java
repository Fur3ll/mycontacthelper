package com.example.dib.logintest.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dib.logintest.ContactAdapter;
import com.example.dib.logintest.base.BaseNavigationActivity;
import com.example.dib.logintest.R;
import com.example.dib.logintest.model.UserModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



public class HomeActivity extends BaseNavigationActivity {

    ContactAdapter contactAdapter;
    RecyclerView itemsRv;

    private List<UserModel> userContacts = new ArrayList<>();


    private void getContactsList(){
        userContacts.clear();
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userContacts.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    UserModel contact = postSnapshot.getValue(UserModel.class);
                    userContacts.add(contact);
                }
                contactAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavTitle("Home");

        String currentUserID = mCurrentUser.getUid();
        itemsRv = (RecyclerView) findViewById(R.id.contact_rv);
        itemsRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        mDatabase = FirebaseDatabase.getInstance().getReference(currentUserID);
        contactAdapter = new ContactAdapter(userContacts,getApplicationContext());
        itemsRv.setAdapter(contactAdapter);
        getContactsList();



    }




}
