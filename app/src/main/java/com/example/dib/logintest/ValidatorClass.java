package com.example.dib.logintest;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dib on 11/24/2017.
 */

public class ValidatorClass extends Activity {

    public final static int PASSWORD_LENGTH = 6;

    public boolean emailValidator(String email){
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean phoneValidator(String phone){
        Pattern pattern;
        Matcher matcher;
        final String PHONE_PATTER = "^((\\+3|8|0)+([0-9]){9})$";
        pattern = Pattern.compile(PHONE_PATTER);
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }


    public boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isValidLengthPassword(String password) {
        if (password.length() >= PASSWORD_LENGTH) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isAnnyDigitsPassword(String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "\\d+";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean isAnnyUpperCaseCharacters(String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "\\w*[A-Z]\\w*";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean passwordFormatError(String password) {
        if (TextUtils.isEmpty(password)) {
            showToast("Чёт пусто");
            return true;
        }else if(password.length() < PASSWORD_LENGTH){
            showToast("Меньше шести символов");
            return true;
        }else if(password.length() > 64){
            showToast("Много символов?");
            return true;
        }

        return false;
    }

    public void showToast(String string) {
        Toast toast = Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
