package com.example.dib.logintest.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fur3ll on 12/19/2017.
 */

public class UserModel implements Parcelable {

    private String name;
    private String email;
    private String phone;
    private String photo;
    private String key;

    public UserModel(){
    }

    public UserModel(String name, String email, String phone){
        this.name = name;
        this.email = email;
        this.phone = phone;
    }

    public UserModel(String name, String email, String phone, String photo){
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.photo = photo;
    }

    public UserModel(String name, String email, String phone, String photo, String key ){
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.photo = photo;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.photo);
        dest.writeString(this.key);
    }

    protected UserModel(Parcel in){
        this.name = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.photo = in.readString();
        this.key = in.readString();
    }

    public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
