package com.example.dib.logintest.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.dib.logintest.R;
import com.example.dib.logintest.utils.CircleTransform;
import com.example.dib.logintest.utils.ValidationHelper;
import com.example.dib.logintest.base.BaseNavigationActivity;
import com.example.dib.logintest.model.UserModel;
import com.example.dib.logintest.utils.Imageutils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;


public class AddContactsActivity extends BaseNavigationActivity {


    @BindView(R.id.add_button)
    Button addBtn;

    @BindView(R.id.til_email)
    TextInputLayout emailTil;
    @BindView(R.id.til_phone)
    TextInputLayout phoneTil;
    @BindView(R.id.til_name)
    TextInputLayout nameTil;

    @BindView(R.id.email)
    TextInputEditText emailTiet;
    @BindView(R.id.phone)
    TextInputEditText phoneTiet;
    @BindView(R.id.name)
    TextInputEditText nameTiet;

    @BindView(R.id.contact_iv)
    ImageView contactIv;

    @BindView(R.id.contact_ib)
    ImageButton contactBtn;

    Imageutils imageutils;

    Uri mContactPhotoUri;
    String photo;
    String mphoto;




    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_contacts;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavTitle("Add contacts");


        imageutils = new Imageutils(this);
        photo = null;
    }


    @OnClick({R.id.add_button,R.id.contact_ib})
    void onClickViews (View view){

        switch (view.getId()){
            case R.id.add_button:

                validateAndAdd();
                finish();
                break;

            case R.id.contact_ib:

                if(imageutils.isDeviceSupportCamera())
                    imageutils.imagepicker(1);
                break;

            default:
                break;
        }
    }



    private void validateAndAdd() {
        ValidationHelper vh = new ValidationHelper();
        vh.addInputView(nameTil, ValidationHelper.ValidationType.MANDATORY);
        vh.addInputView(emailTil, ValidationHelper.ValidationType.EMAIL);
        vh.addInputView(phoneTil, ValidationHelper.ValidationType.PHONE);
        String name = nameTiet.getText().toString();
        String email = emailTiet.getText().toString();
        String phone = phoneTiet.getText().toString();
        String photo = mphoto;

        if(vh.validate()){
            UserModel contact = new UserModel(name,email,phone,photo);
            String currentUserID = mCurrentUser.getUid();
            String userId = mDatabase.push().getKey();
            contact.setKey(userId);
            mDatabase.child(currentUserID).child(userId).setValue(contact);
            startActivity(new Intent(AddContactsActivity.this, HomeActivity.class));
            finish();
        }
    }

    @Override
    public void imageAttachment(int from, String filename, Bitmap file, Uri uri) {

        Picasso.with(this)
                .load(uri)
                .transform(new CircleTransform())
                .fit()
                .centerInside()
                .into(contactIv);


        StorageReference newStorage = mStorage.child("Photos of contacts").child(uri.getLastPathSegment());
        newStorage.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                while (!urlTask.isSuccessful());
                mContactPhotoUri = urlTask.getResult();
                photo = taskSnapshot.getUploadSessionUri().toString();
                mphoto = mContactPhotoUri.toString();

            }
        });
    }
}